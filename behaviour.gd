extends Node2D
 
# Speed of the movement (can be adjusted)

var speed: float = 110
 
# Random movement direction

var direction: Vector2
 
# Timer to track how long the object moves in a single direction

var move_timer: float = 0.0

var change_direction_interval: float = 0.75 # 0.5 seconds
 
var random_angle: float = 0.0

# Called when the node enters the scene tree for the first time
 
# Define boundaries for movement

const MIN_X: float = 0.0 + 30

const MAX_X: float = 1920.0 -30# Adjust based on your screen size

const MIN_Y: float = 0.0 +25

const MAX_Y: float = 1080.0 -110 # Adjust based on your screen size
 
 
var is_grabbed: bool = false

var is_hovered: bool = false
 
# Our guy's class

@export var my_guy_class: int = 3

@export var is_in_his_zone: bool = true
 
 
func _ready():
 
	match my_guy_class:

		0:

			$Sprite2D.texture = preload("res://Assets/class0_guy.png")

		1:

			$Sprite2D.texture = preload("res://Assets/class1_guy.png")

		2:

			$Sprite2D.texture = preload("res://Assets/class2_guy.png")  # Placeholder for class 2

		3:

			$Sprite2D.texture = preload("res://Assets/class3_guy.png") # Placeholder for class 3
 
	randomize_direction()


# Called every frame. 'delta' is the elapsed time since the previous frame.

func _process(delta):

	# Update the timer

	move_timer += delta

	if (!is_grabbed):

		# Check if the timer exceeds the interval (0.5 seconds)

		if move_timer >= change_direction_interval:

			randomize_direction()

			move_timer = 0.0 # Reset the timer

		# Move the icon in the chosen direction at a constant speed

		position += direction * speed * delta

		# Clamp the position to keep the icon within the defined boundaries

		position.x = clamp(position.x, MIN_X, MAX_X)

		position.y = clamp(position.y, MIN_Y, MAX_Y)

	else:

		global_position = get_global_mouse_position()

		move_timer = 0.0
 
func _input(event: InputEvent) -> void:

	# Check for mouse button input events

	if event is InputEventMouseButton:

		if event.pressed:

			if event.button_index == MOUSE_BUTTON_LEFT and is_hovered:

				is_grabbed = true  # Start grabbing the object
 
				scale.x += 0.2

				scale.y += 0.2

				$AudioStreamPlayer2D.play()
				$AnimationPlayer.play("grabbed")
		elif event.button_index == MOUSE_BUTTON_LEFT and !event.pressed and is_grabbed:

			is_grabbed = false  # Release the object

			$AnimationPlayer.stop()

			scale.x = 0.5

			scale.y = 0.5


# Function to randomize direction

func randomize_direction():

		# Free movement between -PI/2 and PI/2 when not at boundaries

	var random_offset = randf_range(-PI / 2, PI / 2) # Limit the turn to ±90 degrees

	random_angle += random_offset  # Update the current angle with the offset

	# Update the direction using the new angle

	direction = Vector2(cos(random_angle), sin(random_angle))
 
 
func _on_area_2d_mouse_entered() -> void:

	is_hovered = true;
 
 
func _on_area_2d_mouse_exited() -> void:

	is_hovered =  false
 
 
func _on_area_2d_area_entered(area: Area2D) -> void:
	print("Area entered !")
	if area.guy_class == my_guy_class:
		print("reduce speed")
		speed = 100
	else:
		speed = 225

 
