extends Node2D


func _on_bottom_left_area_entered(area):
	if area.get_parent().my_guy_class == 3:
		if area.get_parent().is_grabbed:
			$AudioStreamPlayer2D.play()
			area.get_parent().speed = 40
			get_parent().increment_score()
	elif area.get_parent().is_grabbed:
		$AudioStreamPlayer2D.play()
		get_parent().decrement_score()
	else:
		area.get_parent().speed = 90
			


func _on_top_left_area_entered(area):
	if area.get_parent().my_guy_class == 0:
		if area.get_parent().is_grabbed:
			$AudioStreamPlayer2D.play()
			area.get_parent().speed = 40
			get_parent().increment_score()
	elif area.get_parent().is_grabbed:
		$AudioStreamPlayer2D.play()
		get_parent().decrement_score()
	else:
		area.get_parent().speed = 90


func _on_bottom_right_area_entered(area):
	if area.get_parent().my_guy_class == 2:
		if area.get_parent().is_grabbed:
			$AudioStreamPlayer2D.play()
			area.get_parent().speed = 40
			get_parent().increment_score()
	elif area.get_parent().is_grabbed:
		$AudioStreamPlayer2D.play()
		get_parent().decrement_score()
	else:
		area.get_parent().speed = 90
	

func _on_top_right_area_entered(area):
	if area.get_parent().my_guy_class == 1:
		if area.get_parent().is_grabbed:
			$AudioStreamPlayer2D.play()
			area.get_parent().speed = 40
			get_parent().increment_score()
	elif area.get_parent().is_grabbed:
		$AudioStreamPlayer2D.play()
		get_parent().decrement_score()
	else:
		area.get_parent().speed = 90
