extends Node2D

@export var total_time = 180
var score = 0
var grabbed_guy : Node2D = null

func _ready():
	%ProgressBar.max_value = total_time
	%Timer.wait_time = total_time

func _process(delta):
	%ProgressBar.value = %Timer.time_left

func increment_score():
	score += 1
	%ScoreLabel.text = "score : %s" % score
	$UI/AnimationPlayer.play("show_score")
	
func decrement_score():
		score -= 1
		%ScoreLabel.text = "score : %s" % score
		$UI/AnimationPlayer.play("decrement_score")
	
func _input(event):
	if event is InputEventMouseButton and event.is_pressed:
			Input.set_custom_mouse_cursor(preload("res://Assets/hand_thin_closed.png"))
	if event is InputEventMouseButton and event.is_released():
			Input.set_custom_mouse_cursor(preload("res://Assets/hand_thin_open.png"))
	if event.is_action_pressed("quit_game"):
		get_tree().quit()


func _on_timer_timeout():
	get_tree().paused = true
	$CanvasLayer2.visible = true
	%FinalLabel.text = "Great Job !\n
	Your score : %s" % score
